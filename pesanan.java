class pesanan extends kue {
    private double Berat;

    public pesanan(String name, double price, double Berat) {
        // Constructor readystock memanggil constructor superclass kue menggunakan
        // keyword super dengan parameter name dan price.
        super(name, price);
        // Nilai atribut Berat diinisialisasi dengan nilai dari parameter Berat menggunakan keyword this.
        this.Berat = Berat;
    }

    // Membuat method setBerat untuk mengubah nilai atribut Jumlah dengan nilai
    // yang baru.
    public void setBerat(double Berat) {
        this.Berat = Berat;
    }

    // Membuat method getBerat untuk mengembalikan nilai dari atribut Berat.
    public double getBerat() {
        return Berat;
    }

    // Method hitungHarga, untuk menghitung harga kue pesanan dengan cara mengalikan
    // getPrice() dengan Berat.
    public double hitungHarga() {
        return getPrice() * Berat;
    }

    // Method Berat untuk mengembalikan nilai atribut Berat.
    public double Berat() {
        return Berat;
    }

    // Method Jumlah mengembalikan nilai 0, karena readystock tidak memiliki atribut
    // Jumlah.
    public double Jumlah() {
        return 0;
    }
}