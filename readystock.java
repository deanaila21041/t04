class readystock extends kue {
    private double Jumlah;

    public readystock(String name, double price, double Jumlah) {
        // Constructor readystock memanggil constructor superclass kue menggunakan
        // keyword super dengan parameter name dan price.
        super(name, price);
        // nilai atribut Jumlah diinisialisasi dengan nilai dari parameter Jumlah menggunakan keyword this.
        this.Jumlah = Jumlah;
    }

    // Membuat method setJumlah untuk mengubah nilai atribut Jumlah dengan nilai
    // yang baru.
    public void setJumlah(double Jumlah) {
        this.Jumlah = Jumlah;
    }

    // Membuat method getJumlah untuk mengembalikan nilai dari atribut Jumlah.
    public double getJumlah() {
        return Jumlah;
    }

    // Method hitungHarga, untuk menghitung harga kue pesanan dengan cara mengalikan
    // getPrice() dengan Jumlah dan dikalikan 2.
    public double hitungHarga() {
        return getPrice() * Jumlah * 2;
    }

    // Method Berat mengembalikan nilai 0, karena readystock tidak memiliki atribut
    // Berat.
    public double Berat() {
        return 0;
    }

    // Method Jumlah untuk mengembalikan nilai atribut Jumlah.
    public double Jumlah() {
        return Jumlah;
    }
}